package com.gopal151295.uberclone;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

public class ChooseRole extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    private static final String TAG = "ChooseRole";
    private Switch aSwitch;
    private Button proceedButton;
    private Boolean isRider = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_role);

        aSwitch = (Switch) findViewById(R.id.switch1);
        proceedButton = (Button) findViewById(R.id.activity_choose_role_proceedButton);
        aSwitch.setOnCheckedChangeListener(this);
        proceedButton.setOnClickListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked){
            isRider = true;
            proceedButton.setText("PROCEED AS RIDER");
        }
        else{
            isRider = false;
            proceedButton.setText("PROCEED AS DRIVER");

        }
    }

    @Override
    public void onClick(View view) {
        if (Backendless.UserService.CurrentUser() != null){
            BackendlessUser user = Backendless.UserService.CurrentUser();
            user.setProperty("isRider", isRider);
            Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {
                @Override
                public void handleResponse(BackendlessUser response) {
                    generateLog(response);
                    generateToast(response);
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    generateLog(fault);
                    generateToast(fault);
                }
            });

            if ((Boolean) Backendless.UserService.CurrentUser().getProperty("isRider")){
                Intent intent = new Intent(ChooseRole.this, Rider.class);
                startActivity(intent);
                finish();
            }else{
                //start activity for driver
                Intent intent = new Intent(ChooseRole.this, Driver.class);
                startActivity(intent);
                finish();
            }
        }
    }

    private void generateToast(BackendlessFault fault) {
        Toast.makeText(this, "fault: " + fault.getMessage(), Toast.LENGTH_SHORT).show();
    }

    private void generateLog(BackendlessFault fault) {
        Log.i(TAG, "fautl: " + fault.toString());
    }

    private void generateToast(BackendlessUser response) {
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
    }

    private void generateLog(BackendlessUser response) {
        Log.i(TAG, "success: " +response.toString());
    }
}
