package com.gopal151295.uberclone;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.geo.GeoPoint;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Map;

public class Rider extends FragmentActivity implements OnMapReadyCallback, LocationListener, GoogleMap.OnMapLongClickListener, View.OnClickListener {

    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1;
    private static final String TAG = "Rider";
    private GoogleMap mMap;
    private TextView status;
    private Button requestButton;
    private Button cancelButton;
    private double currentLat= 28.69995, currentLng = 77.27345;
    private String provider;
    private GeoPoint currentGeoPoint = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider);

        status = (TextView) findViewById(R.id.activity_rider_status);
        requestButton = (Button) findViewById(R.id.activity_rider_request);
        cancelButton = (Button) findViewById(R.id.activity_rider_cancel);

        status.setVisibility(View.GONE);
        requestButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);

        requestButton.setVisibility(View.VISIBLE);
        cancelButton.setVisibility(View.GONE);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        provider = manager.getBestProvider(new Criteria(), false);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        manager.requestLocationUpdates(provider, MIN_TIME, MIN_DISTANCE, this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLongClickListener(this);

        // Add a marker in Sydney and move the camera
        LatLng myLoc = new LatLng(28.69995, 77.27345);
        mMap.addMarker(new MarkerOptions().position(myLoc));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(myLoc));
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(myLoc, 14);
        mMap.animateCamera(update);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null){
            LatLng latLng = new LatLng(location.getLatitude(),
                    location.getLongitude());
            mMap.clear();
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,14));
            mMap.addMarker(new MarkerOptions().position(latLng).title("Your Location"));

            currentLat = location.getLatitude();
            currentLng = location.getLongitude();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        currentLat = latLng.latitude;
        currentLng = latLng.longitude;
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng));
    }

    @Override
    public void onClick(View view) {
        if (view == requestButton){
            postNewRequest();
            requestButton.setVisibility(View.GONE);
            cancelButton.setVisibility(View.VISIBLE);
            status.setVisibility(View.GONE);
        }else{
            cancelRequest();
            requestButton.setVisibility(View.VISIBLE);
            cancelButton.setVisibility(View.GONE);
            status.setVisibility(View.VISIBLE);
            status.setText("Ride cancelled");
        }

    }

    private void cancelRequest() {

        Backendless.Geo.removePoint(currentGeoPoint, new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.i(TAG, "handleResponse: remove");
                generateLog(fault);
                generateToast(fault);
            }
        });

        Backendless.Data.of("Requests").findLast(new AsyncCallback<Map>() {
            @Override
            public void handleResponse(Map response) {
                Backendless.Data.of("Requests").remove(response, new AsyncCallback<Long>() {
                    @Override
                    public void handleResponse(Long response) {
                        Log.i(TAG, "handleResponse: " + response.toString());
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        generateLog(fault);
                        generateToast(fault);
                    }
                });
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                generateToast(fault);
            }
        });




    }

    private void postNewRequest() {

        GeoPoint geoPoint = new GeoPoint(currentLat, currentLng);
        geoPoint.addCategory("UsersLoc");
        geoPoint.addMetadata("RequesterName", Backendless.UserService.CurrentUser().getProperty("userName"));

        Requests request = new Requests();
        request.setUserName(String.valueOf(Backendless.UserService.CurrentUser().getProperty("userName")));
        request.setLatitude(currentLat);
        request.setLongitude(currentLng);
        request.setGeoPoint(geoPoint);


        Backendless.Data.of(Requests.class).save(request, new AsyncCallback<Requests>() {
            @Override
            public void handleResponse(Requests response) {
                currentGeoPoint = response.getGeoPoint();
                generateLog(response);
                generateToast(response);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                generateLog(fault);
                generateToast(fault);
            }
        });
    }

    private void generateToast(BackendlessFault fault) {
        Toast.makeText(this, "fault: " + fault.getMessage(), Toast.LENGTH_SHORT).show();
    }

    private void generateLog(BackendlessFault fault) {
        Log.i(TAG, "fautl: " + fault.toString());
    }

    private void generateToast(Requests response) {
        Toast.makeText(this, "Success" + response.toString(), Toast.LENGTH_SHORT).show();
    }

    private void generateLog(Requests response) {
        Log.i(TAG, "success: " + response.toString() +response.toString());
    }

    private void generateLog(Void fault) {
        Log.i(TAG, "fautl: " + fault.toString());
    }

    private void generateToast(Void response) {
        Toast.makeText(this, "Success" + response.toString(), Toast.LENGTH_SHORT).show();
    }

}
