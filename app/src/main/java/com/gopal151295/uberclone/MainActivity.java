package com.gopal151295.uberclone;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String APP_ID= "948F3FB7-C71A-CDDB-FFB2-C226A2C3D700";
    private static final String APP_SECRET_KEY="7A508CD8-9827-1D7C-FFF4-1E3821EDD200";
    private static final String APP_VERSION = "v1";
    private static final String TAG = "MainActivity";

    private Button signUpButton;
    private Button loginButton;
    private TextView signUpText;
    private TextView loginText;

    private EditText usernameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;

    private String username;
    private String email;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Backendless.initApp(this, APP_ID, APP_SECRET_KEY, APP_VERSION);

        signUpButton = (Button) findViewById(R.id.activity_main_signupButton);
        loginButton = (Button) findViewById(R.id.activity_main_loginButton);
        signUpText= (TextView) findViewById(R.id.activity_main_signupText);
        loginText = (TextView) findViewById(R.id.activity_main_loginText);

        usernameEditText = (EditText) findViewById(R.id.activity_main_detail_username);
        emailEditText = (EditText) findViewById(R.id.activity_main_detail_email);
        passwordEditText = (EditText) findViewById(R.id.activity_main_detail_password);

        loginButton.setVisibility(View.VISIBLE);
        signUpButton.setVisibility(View.GONE);
        loginText.setVisibility(View.GONE);
        signUpText.setVisibility(View.VISIBLE);

        emailEditText.setVisibility(View.GONE);

        loginButton.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        loginText.setOnClickListener(this);
        signUpText.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == loginText){
            loginButton.setVisibility(View.VISIBLE);
            signUpButton.setVisibility(View.GONE);
            loginText.setVisibility(View.GONE);
            signUpText.setVisibility(View.VISIBLE);
            emailEditText.setVisibility(View.GONE);

            usernameEditText.setText("");
            passwordEditText.setText("");
        }
        else if(view == signUpText){
            loginButton.setVisibility(View.GONE);
            signUpButton.setVisibility(View.VISIBLE);
            loginText.setVisibility(View.VISIBLE);
            signUpText.setVisibility(View.GONE);
            emailEditText.setVisibility(View.VISIBLE);

            usernameEditText.setText("");
            emailEditText.setText("");
            passwordEditText.setText("");
        }
        else if (view == loginButton){
            doLogin();
        }
        else{
            doSignUp();
        }
    }

    private void doSignUp() {
        username = String.valueOf(usernameEditText.getText());
        email = String.valueOf(emailEditText.getText());
        password = String.valueOf(passwordEditText.getText());

        BackendlessUser user = new BackendlessUser();
        user.setProperty("userName", username);
        user.setEmail(email);
        user.setPassword(password);
        Backendless.UserService.register(user, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                generateLog(response);
                generateToast(response);

            }

            @Override
            public void handleFault(BackendlessFault fault) {
                generateLog(fault);
                generateToast(fault);
            }
        });
    }

    private void doLogin() {
        username = String.valueOf(usernameEditText.getText());
        password = String.valueOf(passwordEditText.getText());

        Backendless.UserService.login(username, password, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                generateLog(response);
                generateToast(response);

                Intent intent = new Intent(MainActivity.this, ChooseRole.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                generateLog(fault);
                generateToast(fault);
            }
        }, true);
    }

    private void generateToast(BackendlessFault fault) {
        Toast.makeText(this, "fault: " + fault.getMessage(), Toast.LENGTH_SHORT).show();
    }

    private void generateLog(BackendlessFault fault) {
        Log.i(TAG, "fautl: " + fault.toString());
    }

    private void generateToast(BackendlessUser response) {
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
    }

    private void generateLog(BackendlessUser response) {
        Log.i(TAG, "success: " +response.toString());
    }


}
