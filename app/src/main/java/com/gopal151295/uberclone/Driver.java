package com.gopal151295.uberclone;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class Driver extends AppCompatActivity  implements LocationListener{
    private static final String TAG = "Driver";
    private ListView listView;
    private ArrayList<String> requestsList;
    private ArrayAdapter<String> adapter;
    private GeoPoint currentGeoPoint = null;
    private  ArrayList<Double> distancList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);

        listView = (ListView) findViewById(R.id.activity_driver_listView);
        requestsList = new ArrayList<>();
        adapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_list_item_1, requestsList);
        listView.setAdapter(adapter);

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String provider = manager.getBestProvider(new Criteria(), false);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        manager.requestLocationUpdates(provider, 400, 1, this);

        Location currentLocation = manager.getLastKnownLocation(provider);

        currentGeoPoint = new GeoPoint(currentLocation.getLatitude(),currentLocation.getLongitude());

        Toast.makeText(this, "" + currentGeoPoint.toString(), Toast.LENGTH_SHORT).show();

        BackendlessDataQuery query = new BackendlessDataQuery();
        String whereClause = "driverName = 'null'";
        query.setWhereClause(whereClause);

       distancList = new ArrayList<>();

        Backendless.Data.of("Requests").find(query, new AsyncCallback<BackendlessCollection<Map>>() {
            @Override
            public void handleResponse(BackendlessCollection<Map> response) {
                for (int i = 0; i < response.getCurrentPage().size(); i++) {

                    GeoPoint requestGeoPoint = (GeoPoint) response.getCurrentPage().get(i).get("geoPoint");

                    double distance = calculateDistance(currentGeoPoint.getLatitude(), currentGeoPoint.getLongitude(),
                            requestGeoPoint.getLatitude(), requestGeoPoint.getLongitude(), "K");

                    distance = distance *100;
                    distance = (double)((int)distance);
                    distance = distance/100;

                    distancList.add(distance);
                }

                Collections.sort(distancList);

                for (int i = 0; i < distancList.size(); i++) {
                    requestsList.add(String.valueOf(distancList.get(i)));
                }


                adapter.notifyDataSetChanged();
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    private double calculateDistance(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit.equals("K")) {
            dist = dist * 1.609344;
        } else if (unit.equals("N")) {
            dist = dist * 0.8684;
        }

        return (dist);
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null){
            currentGeoPoint.setLatitude(location.getLatitude());
            currentGeoPoint.setLongitude(location.getLongitude());
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
